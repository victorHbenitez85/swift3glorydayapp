//
//  MemoriesCollectionViewController.swift
//  Glory Days App
//
//  Created by Victor Hugo Benitez Bosques on 03/10/16.
//  Copyright © 2016 Victor Hugo Benitez Bosques. All rights reserved.
//

import UIKit
import AVFoundation
import Photos
import Speech

private let reuseIdentifier = "Cell"

class MemoriesCollectionViewController: UICollectionViewController {
    
    var memories : [URL] = []  //array que contendra la coleccion de memorias por medio de la ruta de cada uno de ellos

    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadMemories() //cargamos todos los recursos contenidos en la estructura de ficheros
        self.collectionView!.register(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) { //usar animaciones y transiciones
        super.viewDidAppear(animated)
        
        self.checkForGrantedPermission() //verifica permisos y en caso contrario muestra la vista de los permisos faltantes
    }
    
    
    func checkForGrantedPermission() {
        
        let phothoAuth : Bool = PHPhotoLibrary.authorizationStatus() == .authorized //verifica autorizacion permiso galeria de fotos
        let recordingAuth : Bool = AVAudioSession.sharedInstance().recordPermission() == .granted //fue dada la autorizacion de uso microfono
        let transcriptionAuth : Bool = SFSpeechRecognizer.authorizationStatus() == .authorized  //status autorizacion speech
        
        let authorized = phothoAuth && recordingAuth && transcriptionAuth   //boolean de las tres autorizaciones
        
        if !authorized{  //caso contrario muestra la vista de las peticiones
            if let vc = storyboard?.instantiateViewController(withIdentifier: "showTerms"){  //instancia del viewcontroller a mostrar
                navigationController?.present(vc, animated: true, completion: nil)  //se presenta la vista
            }
        }
    }
    
    
    /*Se debe realizar una buena gestion de los archivos para evitar duplicarlos y 
     saturar la memori del dispositivo para no despesdiciar espacio*/
    func loadMemories(){
        //eliminar las memorias existentes
        self.memories.removeAll() //eliminamos los objetos que contenga el array evitar duplicados
        
        guard let files = try? FileManager.default.contentsOfDirectory(at: getDocumentDirectory(), includingPropertiesForKeys: nil, options: [])//devolvera un counjunto se archivos
        else{
            return
        }
        
        for file in files{
            let fileName = file.lastPathComponent  //url de la direccion de los archivos
    
            //eliminamos las extensiones de los archivos llamados
            if fileName.hasSuffix(".thumb"){  //buscamos los archivos con el suffix elegido
                let noExtension = fileName.replacingOccurrences(of: ".thumb", with: "") //remplazamos por caracter vacio
                let memoryPath = getDocumentDirectory().appendingPathComponent(noExtension) //creamos directorios con el nombres de los archivos
                
                memories.append(memoryPath) //almacenamos la url en el array
            }
        }
        
        collectionView?.reloadSections(IndexSet(integer: 1))  //recargamos la seccion numero 1 de la collection view controller
    }
    
    //obtener la direccion dond se guradaran los archivos
    func getDocumentDirectory() -> URL{
        
        let path = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask) //userDomainMask es la direccion home del usuario :: documentDirectory path indicado para la carpeta Documents
        let documentsDirectory = path[0] //como toma varias carpetas escojemos la primera la posicion del index 0
        
        return documentsDirectory //retornamos el path donde se guardaran los archivo creados
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 0
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return 0
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
    
        // Configure the cell
    
        return cell
    }

    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */

}
