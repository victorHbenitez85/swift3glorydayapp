//
//  ViewController.swift
//  Glory Days App
//
//  Created by Victor Hugo Benitez Bosques on 03/10/16.
//  Copyright © 2016 Victor Hugo Benitez Bosques. All rights reserved.
//

import UIKit
import Photos
import AVFoundation  //uso del microfono
import Speech

class ViewController: UIViewController {

    @IBOutlet var infoLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    

    @IBAction func askForPermissions(_ sender: UIButton) {
        self.askForPhotosPermissions() //iniciamos la peticion de permisos
        
        
    }
    
    
    
    func askForPhotosPermissions(){ //solicita permisos para uso de la galeria de fotos dispositivo
        
        PHPhotoLibrary.requestAuthorization { [unowned self] (authStatus) in  //se encarga de la peticion de la autorizacion boolean : authState
            
            DispatchQueue.main.async{  //envia la ejecucion de este codigo al hilo principal el cual se encargara de ejecutarlo : so se ejecuta en hilos secundarios
                if authStatus == .authorized{   //se verifica status de la peticion
                    self.askForRecordPermissions()  //se solicita el perimiso de uso del microfono usar self
                }else{
                    self.infoLabel.text = "Nos haz denegado el permiso de fotos. Por favor activalo en los ajustes de tu dispositivo para continuar" //se actualiza el label de petición
                }
            }
        }
    }

    
    func askForRecordPermissions(){  //permisos de uso de microfono
        
        AVAudioSession.sharedInstance().requestRecordPermission { [unowned self] (allowed) in //permisp para usar el microfono de grabacion
            DispatchQueue.main.async {
                if allowed{
                    self.askForTranscriptionPermissions()
                }else{
                    self.infoLabel.text = "Nos haz denegado los permisos de grabacion de audio, por favor activalos en los ajustes de tu dispositivo para continuar"
                }
            }
        }
    }
    
    func askForTranscriptionPermissions() { //permiso para usar speech regonizer
        SFSpeechRecognizer.requestAuthorization { [unowned self] (authStatus) in
            DispatchQueue.main.async {
                if authStatus == .authorized{
                    self.authorizationCompleted()
                }else{
                    self.infoLabel.text = "Nos haz denegado los permisos de transcripcion de texto. por favor activalos en los ajustes del dispositivo para continuar"
                }
            }
        }
    }
    
    
    func authorizationCompleted() {
        dismiss(animated: true, completion: nil) //oculta el viewController usando un segue modally
    }

}

